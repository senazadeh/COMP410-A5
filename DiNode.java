package DiGraph_A5;

import java.util.HashMap;

public class DiNode {
	
	private long idNum;
	private String label;
	private long length = 0;
	
	HashMap<String, DiEdge> incoming = new HashMap<String, DiEdge>();
	HashMap<String, DiEdge> outgoing = new HashMap<String, DiEdge>();

	public DiNode(long idNum, String label) {
		this.idNum = idNum;
		this.label = label;;
	}
	 
	public long getIdNum() { return idNum; }
	public String getLabel() { return label; }
	public long getLength() { return length; }
	public long setLength(long length) { return this.length = length; }
	
}
