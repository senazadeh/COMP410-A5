package DiGraph_A5;

import java.util.Comparator;

public class VexCompare implements Comparator<DiNode>{

	@Override
	public int compare(DiNode n1, DiNode n2) {
		if (n1.getLength() > n2.getLength()) return 1;
		return -1;
	}
}
