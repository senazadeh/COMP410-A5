package DiGraph_A5;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.PriorityQueue;

public class DiGraph implements DiGraphInterface {

	HashMap<String, DiNode> nodes = new HashMap<String, DiNode>();
	
	HashSet<Long> nodeIdNums = new HashSet<Long>();
	HashSet<Long> edgeIdNums = new HashSet<Long>();
	
	int numNodes = 0;
	int numEdges = 0;
	 
  public DiGraph () { // default constructor
    // explicitly include this
    // we need to have the default constructor
    // if you then write others, this one will still be there
  }

//  addNode
//  in: unique id number of the node (0 or greater)
//      string for name
//        you might want to generate the unique number automatically
//        but this operation allows you to specify any integer
//        both id number and label must be unique
//  return: boolean
//            returns false if node number is not unique, or less than 0
//            returns false if label is not unique (or is null)
//            returns true if node is successfully added 
  
	public boolean addNode(long idNum, String label) {

		if (idNum < 0) return false;
		if (nodeIdNums.contains(idNum)) return false;
		if (nodes.containsKey(label)) return false;
		if (label == null) return false;
		
		DiNode newNode = new DiNode(idNum, label);
		nodes.put(label, newNode);
		nodeIdNums.add(idNum);
		
		numNodes++;
		return true;
	}    

//	addEdge
//	in: unique id number for the new edge, 
//	    label of source node,
//	    label of destination node,
//	    weight for new edge (use 1 by default)
//	    label for the new edge (allow null)
//	return: boolean
//	          returns false if edge number is not unique or less than 0
//	          returns false if source node is not in graph
//	          returns false if destination node is not in graph
//	          returns false is there already is an edge between these 2 nodes
//	          returns true if edge is successfully added 
    
	public boolean addEdge(long idNum, String sLabel, String dLabel, long weight, String eLabel) {

		if (idNum < 0) return false;
		if (edgeIdNums.contains(idNum)) return false;
		if (!nodes.containsKey(sLabel)) return false;
		if (!nodes.containsKey(dLabel)) return false;
		if (nodes.get(sLabel).outgoing.containsKey(dLabel)) return false;
 
		DiEdge newEdge = new DiEdge(idNum, sLabel, dLabel, weight, eLabel);
		nodes.get(dLabel).incoming.put(sLabel, newEdge);
		nodes.get(sLabel).outgoing.put(dLabel, newEdge);
		edgeIdNums.add(idNum);
		 
		numEdges++;
		return true;
	}
	
//    delNode
//    in: string 
//          label for the node to remove
//    out: boolean
//           return false if the node does not exist
//           return true if the node is found and successfully removed

	public boolean delNode(String label) {

		if (!nodes.containsKey(label)) return false;
		DiNode del = nodes.get(label);
		
		for (Map.Entry<String, DiEdge> entry: del.incoming.entrySet()) {
			DiNode current = nodes.get(entry.getKey());
			edgeIdNums.remove(current.outgoing.get(label).getIdNum());
			current.outgoing.remove(label);
			numEdges--;
		}
		for (Map.Entry<String, DiEdge> entry: del.outgoing.entrySet()) {
			DiNode current = nodes.get(entry.getKey());
			edgeIdNums.remove(current.incoming.get(label).getIdNum());
			current.incoming.remove(label);
			numEdges--;
		}
		
		nodeIdNums.remove(nodes.get(label).getIdNum());
		nodes.remove(label);
		numNodes--;
		return true;
	} 
	
//    delEdge 
//    in: string label for source node
//        string label for destination node
//    out: boolean
//           return false if the edge does not exist
//           return true if the edge is found and successfully removed
      		   
	public boolean delEdge(String sLabel, String dLabel) {

		if (!nodes.containsKey(sLabel)) return false;
		if (!nodes.containsKey(dLabel)) return false;
		if (!nodes.get(sLabel).outgoing.containsKey(dLabel)) return false;
		
		
		edgeIdNums.remove(nodes.get(sLabel).outgoing.get(dLabel).getIdNum());
		nodes.get(sLabel).outgoing.remove(dLabel);
		
		numEdges--;
		return true;
	}
	
//	   numNodes
//	      in: nothing
//	      return: integer 0 or greater
//	                reports how many nodes are in the graph
	                
	public long numNodes() {
		return numNodes;
	}
	
//	   numEdges
//	      in: nothing
//	      return: integer 0 or greater
//	                reports how many edges are in the graph
	                
	public long numEdges() {
		return numEdges;
	}
	
//		shortestPath:
//		      in: string label for start vertex
//		      return: array of ShortestPathInfo objects (ShortestPathInfo)
//		              length of this array should be numNodes (as you will put in all shortest 
//		              paths including from source to itself)
//		              See ShortestPathInfo class for what each field of this object should contain
	
	public ShortestPathInfo[] shortestPath(String label) {

		ShortestPathInfo[] paths = new ShortestPathInfo[numNodes];
		PriorityQueue<DiNode> pQ = new PriorityQueue<DiNode>(20, new VexCompare());
		
		DiNode start = nodes.get(label);
		start.setLength(0);
		pQ.add(start);
		
		for (DiNode entry: nodes.values()) {
			if (entry == start) {
				continue;
			} else {
				entry.setLength(Long.MAX_VALUE);
				pQ.add(entry);
			}
		}
		
		for (int i = 0; i < paths.length; i++) {
			dijKstraAlgorithm(pQ, i, paths);
		}
		
		return paths;
	}
	
	public void dijKstraAlgorithm(PriorityQueue<DiNode> pQ, int i, ShortestPathInfo[] paths) {
		
		ShortestPathInfo path;
		DiNode current = pQ.remove();
		
		if (current.getLength() == Long.MAX_VALUE) {
			path = new ShortestPathInfo(current.getLabel(), -1);
			paths[i] = path;
			return;
		} else {
			path = new ShortestPathInfo(current.getLabel(), current.getLength());
			paths[i] = path;
		}
		
		for (DiEdge edge : current.outgoing.values()) {
			DiNode node = nodes.get(edge.getDLabel());
			if (edge.getWeight() + current.getLength() < node.getLength()) {
				node.setLength(edge.getWeight() + current.getLength());
				pQ.remove(node);
				pQ.add(node);
			}
			
		}
	}

  // rest of your code to implement the various operations
}
